const express = require("express");
const app = express();
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
require("dotenv").config({ path: `.env.${process.env.NODE_ENV}` });
const axios = require("axios").default;
axios.defaults.baseURL = process.env.REST_API_URL;

const cors = require("cors");
app.use(cors());

const accRouter = require("./routes/acc/acc-router");
app.use("/acc", accRouter);

const staffRouter = require("./routes/staff/staff-router");
app.use("/staff", staffRouter);

const teacherRouter = require("./routes/teacher/teacher-router");
app.use("/teacher", teacherRouter);

app.use("/api", require("./routes/staff/upload-point/upload-point-2"));

app.listen(process.env.PORT, () => {
  console.log("App listening on port " + process.env.PORT);
});
