const express = require("express");
const router = express.Router();
const multer = require("multer");
const upload = multer();
const { authen, author } = require("../../acc/protect-middleware");
const connection = require("../../../db");
const { ROLE } = require("../../acc/role");
const axios = require("axios").default;
const ecies = require("ecies-geth");
var crypto = require("crypto");
const fs = require("fs");
const path = require("path");
const XLSX = require("xlsx");
const Joi = require("joi");

const excelSchema = Joi.object({
  "Mã SV": Joi.number().required(),
  Điểm: Joi.number().required(),
});

const uploadHalfSemesterHandler = async (req, res) => {
  const records = pareExcel(req.file.buffer);
  // console.log(records);
  try {
    saveExcelFile(req.file);
    const { error, value } = excelSchema.validate(records[0], { abortEarly: false, allowUnknown: true });
    if (error) {
      const errors = {};
      for (let err of error.details) {
        errors[err.context.key] = err.message;
      }
      return res.status(400).json({ ok: false, msg: "Please check excel file format!", original: value, errors });
    }
    const classId = req.body.classID;
    const col = (await connection).db().collection("Classes");
    const cls = await col.findOne({ classId: classId });
    const updatedClass = fillHalfSemesterGrade(cls, records);
    const opResult = await col.updateOne({ classId: classId }, { $set: { students: updatedClass.students } });
    res.send({ ok: true });
  } catch (error) {
    console.log(error);
    res.status(500).send(error.toString());
  }
};

const uploadFinalSemesterHandler = async (req, res) => {
  try {
    saveExcelFile(req.file);
    const records = pareExcel(req.file.buffer);
    // console.log(records);

    const { error, value } = excelSchema.validate(records[0], { abortEarly: false, allowUnknown: true });
    if (error) {
      const errors = {};
      for (let err of error.details) {
        errors[err.context.key] = err.message;
      }
      return res.status(400).json({ ok: false, msg: "Please check excel file format!", original: value, errors });
    }
    const classId = req.body.classID;
    const col = (await connection).db().collection("Classes");
    const cls = await col.findOne({ classId: classId });
    const updatedClass = fillFinalSemesterGrade(cls, records);
    const payload = await preparePayload(updatedClass);
    try {
      const response = await axios.post("/create_subjects", payload);
      const updatedTxidClass = addTxid(cls, response.data.transactions);
      const opResult = await col.updateOne({ classId: classId }, { $set: { students: updatedTxidClass.students } });
      res.send({ ok: true, txs: response.data.transactions });
    } catch (error) {
      console.log(error);
      res.status(502).send(error);
    }
  } catch (error) {
    console.log(error);
    res.status(500).json(error.toString());
  }
};

router.post("/half-semester-grade", authen, author(ROLE.STAFF), upload.single("halfSemesterGradeFile"), uploadHalfSemesterHandler);

router.post("/final-semester-grade", authen, author(ROLE.STAFF), upload.single("finalSemesterGradeFile"), uploadFinalSemesterHandler);

router.post("/grade", authen, author(ROLE.STAFF), upload.single("gradeFile"), async (req, res) => {
  try {
    const type = req.body.type;
    if (type === "half-semester") {
      uploadHalfSemesterHandler(req, res);
    } else if (type === "final-semester") {
      uploadFinalSemesterHandler(req, res);
    }
  } catch (error) {
    console.log(error);
    res.status(500).json(error.toString());
  }
});

function saveExcelFile(file) {
  const fileName = Date.now() + "-" + file.originalname;
  fs.writeFileSync(path.join(__dirname, "uploads", fileName), file.buffer);
}

function pareExcel(buffer) {
  const workbook = XLSX.read(buffer, { type: "buffer" });
  const worksheet = workbook.Sheets[workbook.SheetNames[0]];
  const records = XLSX.utils.sheet_to_json(worksheet);
  return records;
}

function fillHalfSemesterGrade(cls, records) {
  records.forEach((record) => {
    const foundStudent = cls.students.find((std) => std.studentId == record["Mã SV"]);
    foundStudent.halfSemesterPoint = record["Điểm"];
  });
  return cls;
}

function fillFinalSemesterGrade(cls, records) {
  records.forEach((record) => {
    const foundStudent = cls.students.find((std) => std.studentId == record["Mã SV"]);
    foundStudent.finalSemesterPoint = record["Điểm"];
  });
  return cls;
}

async function preparePayload(cls) {
  const pointPromises = cls.students
    .filter((student) => student.halfSemesterPoint && student.finalSemesterPoint)
    .map(async (student) => {
      const plain = {
        semester: cls.semester,
        subject: cls.subject,
        classId: cls.classId,
        teacherId: cls.teacher.teacherId,
        teacherName: cls.teacher.name,
        department: cls.teacher.department,
        bureauId: cls.bureau.bureauId,
        bureauName: cls.bureau.name,
        studentId: student.studentId,
        studentName: student.name,
        studentClass: student.class,
        halfSemesterPoint: student.halfSemesterPoint,
        finalSemesterPoint: student.finalSemesterPoint,
      };
      const cipher = (await ecies.encrypt(Buffer.from(student.publicKey65, "hex"), Buffer.from(JSON.stringify(plain)))).toString("hex");
      const hash = crypto.createHash("sha256").update(JSON.stringify(plain)).digest("hex");
      return { studentPublicKey: student.publicKey, studentPublicKey65: student.publicKey65, cipher, hash };
    });
  const points = await Promise.all(pointPromises);
  return { privateKeyHex: cls.teacher.privateKey, universityPublicKey: cls.teacher.universityPublicKey, classId: cls.classId, points };
}

function addTxid(cls, txs) {
  cls.students
    .filter((student) => student.halfSemesterPoint && student.finalSemesterPoint)
    .forEach((std) => {
      std.txid = txs.find((tx) => tx.studentPublicKey === std.publicKey).transactionId;
    });
  return cls;
}
module.exports = router;
