const express = require("express");
const router = express.Router();
const multer = require("multer");
const upload = multer();
const { authen, author } = require("../../acc/protect-middleware");
const connection = require("../../../db");
const { ROLE } = require("../../acc/role");
const axios = require("axios").default;
const ecies = require("ecies-geth");
var crypto = require("crypto");
const fs = require("fs");
const path = require("path");
const XLSX = require("xlsx");
const Joi = require("joi");

const excelSchema = Joi.object({
  ClassID: Joi.number().required(),
  StudentID: Joi.number().required(),
  HalfSemesterGrade: Joi.number().required(),
  FinalSemesterGrade: Joi.number().required(),
});

router.post("/upload-point", authen, author(ROLE.STAFF), upload.single("excel-file"), async (req, res) => {
  try {
    // console.log(req.file);
    saveExcelFile(req.file);
    const records = pareExcel(req.file.buffer);
    const { error, value } = excelSchema.validate(records[0], { abortEarly: false });
    if (error) {
      const errors = {};
      for (let err of error.details) {
        errors[err.context.key] = err.message;
      }
      return res.status(400).json({ ok: false, msg: "Please check excel file format!", original: value, errors });
    }

    const classId_sidAndPointsMap = extractPointOfClasses(records);
    const classes = await getClassesFromDb(Object.keys(classId_sidAndPointsMap)); // also filter class already have points
    fillPoint(classes, classId_sidAndPointsMap);
    const payloads = await preparePayloads(classes); // 1 payload for each class

    // payloads.forEach(async (payload) => {
    //   try {
    //     const response = await axios.post("/create_subjects", payload);
    //     const updateResult = await updateDB(classes, payload, response);
    //     console.log(updateResult.result);
    //   } catch (error) {
    //     console.log(error.response.data);
    //   }
    // });

    const result = await payloads.reduce(
      async (resultAccummulator, payload) => {
        const accum = await resultAccummulator;
        try {
          const response = await axios.post("/create_subjects", payload);
          const updateResult = await updateDB(classes, payload, response);
          accum.updatedClass.push(payload.classId);
          console.log(updateResult.result);
          return accum;
        } catch (error) {
          console.log(error);
          accum.updateFailClass.push(payload.classId);
          accum.msg.push(error.response.data);
          return accum;
        }
      },
      { updatedClass: [], updateFailClass: [], msg: [] }
    );

    res.json(result);
  } catch (error) {
    console.log(error);
    res.status(500).json(error.toString());
  }
});

function saveExcelFile(file) {
  const fileName = Date.now() + "-" + file.originalname;
  fs.writeFileSync(path.join(__dirname, "uploads", fileName), file.buffer);
}

function pareExcel(buffer) {
  const workbook = XLSX.read(buffer, { type: "buffer" });
  const worksheet = workbook.Sheets[workbook.SheetNames[0]];
  const records = XLSX.utils.sheet_to_json(worksheet);
  return records;
}

function extractPointOfClasses(records) {
  return records.reduce((accummulator, record) => {
    accummulator[record.ClassID] = [...(accummulator[record.ClassID] || []), { sid: record.StudentID, halfSemesterGrade: record.HalfSemesterGrade, finalSemesterGrade: record.FinalSemesterGrade }];
    return accummulator;
  }, {});
}

async function getClassesFromDb(classIds) {
  const clsCol = (await connection).db().collection("Classes");
  return clsCol.find({ classId: { $in: classIds } }).toArray();
}

function fillPoint(classes, classId_sidAndPointsMap) {
  classes.forEach((cls) => {
    const sidAndPoints = classId_sidAndPointsMap[cls.classId];
    cls.students.forEach((student) => {
      const pointOfStudent = sidAndPoints.find((item) => item.sid == student.studentId);
      if (pointOfStudent) {
        student.halfSemesterPoint = pointOfStudent.halfSemesterGrade;
        student.finalSemesterPoint = pointOfStudent.finalSemesterGrade;
      }
    });
  });
}

async function preparePayloads(classes) {
  const payloadPromises = classes.map(async (cls) => {
    const pointPromises = cls.students
      .filter((std) => std.halfSemesterPoint)
      .map(async (student) => {
        const plain = {
          semester: cls.semester,
          subject: cls.subject,
          classId: cls.classId,
          teacherId: cls.teacher.teacherId,
          teacherName: cls.teacher.name,
          department: cls.teacher.department,
          bureauId: cls.bureau.bureauId,
          bureauName: cls.bureau.name,
          studentId: student.studentId,
          studentName: student.name,
          studentClass: student.class,
          halfSemesterPoint: student.halfSemesterPoint,
          finalSemesterPoint: student.finalSemesterPoint,
        };
        const cipher = (await ecies.encrypt(Buffer.from(student.publicKey65, "hex"), Buffer.from(JSON.stringify(plain)))).toString("hex");
        const hash = crypto.createHash("sha256").update(JSON.stringify(plain)).digest("hex");
        return { studentPublicKey: student.publicKey, studentPublicKey65: student.publicKey65, cipher, hash };
      });
    const points = await Promise.all(pointPromises);
    return { privateKeyHex: cls.teacher.privateKey, universityPublicKey: cls.teacher.universityPublicKey, classId: cls.classId, points };
  });
  return await Promise.all(payloadPromises);
}

async function updateDB(classes, payload, response) {
  const cls = classes.find((clx) => clx.classId == payload.classId);
  const updatedCls = addTxids(cls, response.data.transactions);
  const clsCol = (await connection).db().collection("Classes");
  return clsCol.updateOne({ classId: cls.classId }, { $set: { students: updatedCls.students } });
}

function addTxids(claxx, txs) {
  claxx.students
    .filter((std) => std.halfSemesterPoint)
    .forEach((student) => {
      student.txid = txs.find((tx) => tx.studentPublicKey === student.publicKey).transactionId;
    });
  return claxx;
}

module.exports = router;
