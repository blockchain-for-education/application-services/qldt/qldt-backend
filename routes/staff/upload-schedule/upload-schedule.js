const express = require("express");
const router = express.Router();
const multer = require("multer");
const upload = multer();
const { authen, author } = require("../../acc/protect-middleware");
const { ROLE } = require("../../acc/role");
const connection = require("../../../db");
const axios = require("axios").default;
const XLSX = require("xlsx");
const { randomBytes } = require("crypto");
const secp256k1 = require("secp256k1");
const generator = require("generate-password");
const bcrypt = require("bcryptjs");

// temp api for dev
router.post("/create-random-bureau", authen, author(ROLE.STAFF), async (req, res) => {
  const rdNumber = Math.floor(Math.random() * 1000);
  const keyPair = genNewKeyPair();
  const bureau = {
    bureauId: "GVu" + rdNumber,
    name: "GiaoVu" + rdNumber,
    email: "giaovu" + rdNumber + "@gmail.com",
    department: "CNTT",
    publicKey: keyPair.publicKey,
    universityPublicKey: "031e9602b11304df8a1d6bd86aeaadead8820fb3fab0335317f63b309f0c1d2682",
  };
  const profiles = [bureau];
  try {
    const createBureauResponse = await axios.post("/create_edu_officers", {
      privateKeyHex: req.body.privateKeyHex,
      profiles: profiles,
    });
    const bureauCol = (await connection).db().collection("Bureau");
    await bureauCol.insertMany(profiles);
    res.json({ ok: true });
  } catch (error) {
    console.log(error);
    res.status(502).send(error);
  }
});

async function createBureau(req) {
  const rdNumber = Math.floor(Math.random() * 1000);
  const keyPair = genNewKeyPair();
  const bureau = {
    bureauId: "GVu" + rdNumber,
    name: "GiaoVu" + rdNumber,
    email: "giaovu" + rdNumber + "@gmail.com",
    department: "CNTT",
    publicKey: keyPair.publicKey,
    universityPublicKey: "031e9602b11304df8a1d6bd86aeaadead8820fb3fab0335317f63b309f0c1d2682",
  };
  const profiles = [bureau];
  try {
    const createBureauResponse = await axios.post("/create_edu_officers", {
      privateKeyHex: req.body.privateKeyHex,
      profiles: profiles,
    });
    const bureauCol = (await connection).db().collection("Bureau");
    await bureauCol.insertMany(profiles);
  } catch (error) {
    console.log(error);
  }
}

router.post("/upload-schedule", authen, author(ROLE.STAFF), upload.single("excel-file"), async (req, res) => {
  try {
    await createBureau(req);
    console.time("Upload Schedule Total time");
    const records = parseExcel(req.file.buffer);
    const createTeacherResult = await createNewTeacher(req, res, records);
    if (createTeacherResult.ok) {
      const createClassResult = await createClasses(req, res, records);
      console.timeEnd("Upload Schedule Total time");
      createClassResult.ok ? res.json({ ok: true }) : res.status(500).json({ ok: false, msg: "error when create classes" });
    } else {
      return res.status(502).send("Error when create Teacher");
    }
  } catch (error) {
    console.log(error);
    return res.status(500).send("Something went wrong,..." + error);
  }
});

router.get("/teachers", async (req, res) => {
  try {
    const col = (await connection).db().collection("Teacher");
    const teachers = await col.find({}).toArray();
    teachers.length > 0 ? res.json(teachers) : res.json([]);
  } catch (error) {
    console.log(error);
    res.status(500).json(error.toString());
  }
});

router.get("/classes/:classId", async (req, res) => {
  try {
    const classId = req.params.classId;
    const col = (await connection).db().collection("Classes");
    const claxx = await col.findOne({ classId });
    res.json(claxx);
  } catch (error) {
    console.log(error);
    res.status(500).json(error.toString());
  }
});

async function createNewTeacher(req, res, records) {
  const teachers = extractTeachers(records);
  const uniqueTeachers = filterDuplicate(teachers);
  let newTeachers = await filterExistedTeacher(uniqueTeachers);
  console.log("number of new teacher: " + newTeachers.length);
  // if nothing new, skip
  if (newTeachers.length === 0) {
    return { ok: true };
  }
  newTeachers = await addUniversityPublicKey(req.user.uid, newTeachers);
  const newTeacherWithKey = addKeyPair(newTeachers);
  const profiles = prepareTeacherProfile(newTeacherWithKey);
  try {
    const response = await axios.post("/create_teachers", {
      privateKeyHex: req.body.privateKeyHex,
      profiles: profiles,
    });
    // if success:
    addTxid(newTeacherWithKey, response.data.transactions);
    addPasswordAndRole(newTeacherWithKey);
    const insertedIds = await createAccount(newTeacherWithKey);
    addUID(newTeacherWithKey, insertedIds);
    const teacherCol = (await connection).db().collection("Teacher");
    const opResult = await teacherCol.insertMany(newTeacherWithKey);
    console.log("create Teacher ok!");
    return { ok: true };
  } catch (error) {
    // if fail
    console.log("create Teacher fail!");
    console.log(error);
    return { ok: false };
  }
}

async function createClasses(req, res, records) {
  const teacherCol = (await connection).db().collection("Teacher");
  const allTeachers = await teacherCol.find({}).toArray();
  const bureauCol = (await connection).db().collection("Bureau");
  const allBureaus = await bureauCol.find({}).toArray();
  const classes = prepareClasses(records, allTeachers, allBureaus);
  const uniqueClasses = filterClassesDuplicate(classes);
  const newClasses = await filterExistedClasses(uniqueClasses);
  console.log("number of new classes: " + newClasses.length);
  // if nothing new, skip
  if (newClasses.length === 0) {
    return { ok: true };
  }
  const classesPayload = prepareClassesPayload(newClasses);
  try {
    await axios.post("/create_classes", {
      privateKeyHex: req.body.privateKeyHex,
      classes: classesPayload,
    });
    const classesCol = (await connection).db().collection("Classes");
    await classesCol.insertMany(newClasses);
    console.log("create classes ok!");
    return { ok: true };
  } catch (error) {
    console.log(error.response.data);
    return { ok: false, mgs: error.response.data };
  }
}

function prepareClasses(records, allTeachers, allBureaus) {
  return records.map((rc) => {
    const teacherId = rc.Email.split("@")[0];
    return {
      semester: rc.Semester,
      classId: rc.ClassID,
      subject: {
        subjectId: rc.CourseID,
        name: rc.ClassName,
        credit: rc.CreditInfo,
        note: rc.ClassNote,
      },
      teacher: allTeachers.find((t) => t.teacherId === teacherId),
      // FIXME: for now, just pick the first one
      bureau: allBureaus[0],
    };
  });
}

function filterClassesDuplicate(classes) {
  return classes.filter((cls, index, self) => self.findIndex((c) => c.classId === cls.classId) === index);
}

async function filterExistedClasses(classes) {
  const classesCol = (await connection).db().collection("Classes");
  const allClasses = await classesCol.find({}).toArray();
  const allClassesId = allClasses.map((cls) => cls.classId);
  return classes.filter((cls) => !allClassesId.includes(cls.classId));
}

function prepareClassesPayload(newClasses) {
  return newClasses.map((cls) => ({
    classId: cls.classId,
    teacherPublicKey: cls.teacher.publicKey,
    bureauPublicKey: cls.bureau.publicKey,
  }));
}

function parseExcel(buffer) {
  const workbook = XLSX.read(buffer, { type: "buffer" });
  const worksheet = workbook.Sheets[workbook.SheetNames[0]];
  const records = XLSX.utils.sheet_to_json(worksheet);
  return records;
}

function extractTeachers(records) {
  return records.map((record) => ({
    teacherId: record.Email.split("@")[0],
    name: record.Teacher,
    email: record.Email,
    department: record.Department,
    phone: record.Phone,
  }));
}

function filterDuplicate(teachers) {
  return teachers.filter((teacher, index, self) => self.findIndex((t) => t.teacherId === teacher.teacherId) === index);
}

async function filterExistedTeacher(teachers) {
  const teacherCol = (await connection).db().collection("Teacher");
  const savedTeachers = await teacherCol.find({}, { projection: { teacherId: 1, _id: 0 } }).toArray();
  const savedTeacherIds = savedTeachers.map((teacher) => teacher.teacherId);
  const newTeachers = teachers.filter((teacher) => !savedTeacherIds.includes(teacher.teacherId));
  return newTeachers;
}

async function addUniversityPublicKey(staffUID, teachers) {
  const uniProfileCol = (await connection).db().collection("UniversityProfile");
  const universityPublicKey = (await uniProfileCol.findOne({ uid: staffUID })).pubkey;
  teachers.forEach((teacher) => (teacher.universityPublicKey = universityPublicKey));
  return teachers;
}

function addKeyPair(teachers) {
  return teachers.map((teacher) => {
    const keyPair = genNewKeyPair();
    return { ...teacher, publicKey: keyPair.publicKey, privateKey: keyPair.privateKey };
  });
}

function genNewKeyPair() {
  let privateKey;
  do {
    privateKey = randomBytes(32);
  } while (!secp256k1.privateKeyVerify(privateKey));
  const publicKey = secp256k1.publicKeyCreate(privateKey);
  return { privateKey: Buffer.from(privateKey).toString("hex"), publicKey: Buffer.from(publicKey).toString("hex") };
}

function prepareTeacherProfile(newTeachers) {
  return newTeachers.map((teacher) => ({
    teacherId: teacher.teacherId,
    name: teacher.name,
    department: teacher.department,
    publicKey: teacher.publicKey,
    universityPublicKey: teacher.universityPublicKey,
  }));
}

function addTxid(newTeachers, transactions) {
  newTeachers.forEach((teacher) => {
    teacher.txid = transactions.find((tx) => tx.teacherId === teacher.teacherId).transactionId;
  });
}

function addPasswordAndRole(newTeachers) {
  newTeachers.forEach((teacher) => {
    let randomPassword = generator.generate({ length: 8, numbers: true });
    teacher.firstTimePassword = randomPassword;
    const salt = bcrypt.genSaltSync();
    let hashedPassword = bcrypt.hashSync(randomPassword, salt);
    teacher.hashedPassword = hashedPassword;
    teacher.role = ROLE.TEACHER;
  });
}

async function createAccount(newTeachers) {
  const accounts = newTeachers.map((teacher) => ({
    email: teacher.email,
    hashedPassword: teacher.hashedPassword,
    role: teacher.role,
  }));
  // insert account
  const accCol = (await connection).db().collection("Account");
  const insertedIds = (await accCol.insertMany(accounts)).insertedIds;
  return insertedIds;
}

function addUID(newTeachers, insertedIds) {
  newTeachers.forEach((teacher, index) => (teacher.uid = insertedIds[index]));
}

module.exports = router;
