const express = require("express");
const router = express.Router();
const multer = require("multer");
const upload = multer();
const { authen, author } = require("../../acc/protect-middleware");
const { ROLE } = require("../../acc/role");
const connection = require("../../../db");
const XLSX = require("xlsx");
const { randomBytes } = require("crypto");
const secp256k1 = require("secp256k1");
const generator = require("generate-password");
const bcrypt = require("bcryptjs");
var path = require("path");

router.post("/test", async (req, res) => {
  // console.time("genSalt");
  // for (let i = 0; i <= 10000; i++) {
  //   bcrypt.genSaltSync();
  // }
  // console.timeEnd("genSalt");
  // --> 10000time ~ 158ms

  // console.time("genrandompw");
  // for (let i = 0; i <= 10000; i++) {
  //   generator.generate({ length: 8, numbers: true });
  // }
  // console.timeEnd("genrandompw");
  // --> 10000time ~ 10ms

  const rdpw = generator.generate({ length: 8, numbers: true });
  const salt = bcrypt.genSaltSync(5);
  console.time("sync");
  for (let i = 0; i <= 8000; i++) {
    bcrypt.hashSync(rdpw, salt);
  }
  console.timeEnd("sync");

  // const arr6000 = [...Array(6000).keys()];
  // console.time("async");
  // const arrayOfPromise = arr6000.map((item) => bcrypt.hash(rdpw, salt));
  // const result = await Promise.all(arrayOfPromise);
  // console.timeEnd("async");

  // console.time();
  // for (let i = 0; i <= 100; i++) {
  //   generator.generate({ length: 8, numbers: true });
  // }
  // console.timeEnd();
  res.send({ ok: "done" });
});

router.get("/intersec-classid", (req, res) => {
  const clswb = XLSX.readFile(path.join(__dirname, "Classes.xls"));
  const clsws = clswb.Sheets[clswb.SheetNames[0]];
  const clsrc = XLSX.utils.sheet_to_json(clsws);
  const clsClassIds = clsrc.map((rc) => rc.ClassID);
  const uniqueClassId1 = [...new Set(clsClassIds)];

  const dsddwb = XLSX.readFile(path.join(__dirname, "ds-diem-danh.xlsx"));
  const dsdhws = dsddwb.Sheets[dsddwb.SheetNames[0]];
  const dsdhrc = XLSX.utils.sheet_to_json(dsdhws);
  const dsddClassIds = dsdhrc.map((rc) => rc.classid);
  const uniqeClassId2 = [...new Set(dsddClassIds)];

  const intersection = uniqueClassId1.filter((classId) => uniqeClassId2.includes(classId));
  console.log({ uniqueClassId1, uniqeClassId2, intersection });
  res.send({ ok: true });
});

router.get("/students", async (req, res) => {
  try {
    const col = (await connection).db().collection("Student");
    const students = await col.find({}).toArray();
    students.length > 0 ? res.json(students) : res.json([]);
  } catch (error) {
    console.log(error);
    res.status(500).json(error.toString());
  }
});

router.post("/upload-attendance", authen, author(ROLE.STAFF), upload.single("excel-file"), async (req, res) => {
  try {
    console.time("Upload Attendance Total time");
    const records = parseExcel(req.file.buffer);
    const students = extractStudent(records);
    const uniqueStudents = filterDuplicate(students);
    let newStudents = await filterExistedStudent(uniqueStudents);
    console.log("number of new student:" + newStudents.length);
    if (newStudents.length !== 0) {
      newStudents = addKey(newStudents);
      console.time("addAcc");
      newStudents = addAccount(newStudents);
      console.timeEnd("addAcc");
      const uids = await createStudentAccount(newStudents);
      newStudents = addUid(newStudents, uids);
      await saveStudents(newStudents);
    }
    const classesAndStudentIds = extractClasses(records);
    console.time("updateClass");
    const opResult = await updateStudentListOfClass(classesAndStudentIds);
    console.timeEnd("updateClass");
    console.timeEnd("Upload Attendance Total time");
    return res.json(opResult);
    //
  } catch (error) {
    console.log(error);
    return res.status(500).send(error);
  }
});

function parseExcel(buffer) {
  const workbook = XLSX.read(buffer, { type: "buffer" });
  const worksheet = workbook.Sheets[workbook.SheetNames[0]];
  const records = XLSX.utils.sheet_to_json(worksheet);
  return records;
}

function extractStudent(records) {
  return records.map((rc) => ({
    studentId: rc.StudentID,
    name: rc.studentname,
    // birthday: rc.birthdate, --> xlsx misunderstand date fortmat from excel
    class: rc.groupname,
    academic: rc.AcademicName,
  }));
}

function filterDuplicate(students) {
  return students.filter((std, index, self) => self.findIndex((s) => s.studentId === std.studentId) === index);
}

async function filterExistedStudent(students) {
  const stdCol = (await connection).db().collection("Student");
  const allStudents = await stdCol.find({}, { projection: { studentId: 1, _id: 0 } }).toArray();
  const allStudentId = allStudents.map((std) => std.studentId);
  return students.filter((std) => !allStudentId.includes(std.studentId));
}

function addKey(students) {
  return students.map((student) => {
    const key = genNewKey();
    return {
      ...student,
      publicKey: key.publicKey,
      privateKey: key.privateKey,
      publicKey65: key.publicKey65,
    };
  });
}

function genNewKey() {
  let privateKey;
  do {
    privateKey = randomBytes(32);
  } while (!secp256k1.privateKeyVerify(privateKey));
  const publicKey = secp256k1.publicKeyCreate(privateKey);
  const publicKey65 = secp256k1.publicKeyCreate(privateKey, false);
  return { privateKey: Buffer.from(privateKey).toString("hex"), publicKey: Buffer.from(publicKey).toString("hex"), publicKey65: Buffer.from(publicKey65).toString("hex") };
}

function addAccount(students) {
  const salt = bcrypt.genSaltSync(1);
  return students.map((student) => {
    const randomPassword = generator.generate({ length: 8, numbers: true });
    return { ...student, account: student.studentId + "@sis.hust.edu.vn", role: ROLE.STUDENT, firstTimePassword: randomPassword, hashedPassword: bcrypt.hashSync(randomPassword, salt) };
  });
}

async function createStudentAccount(newStudents) {
  const accounts = newStudents.map((student) => ({
    email: student.account,
    hashedPassword: student.hashedPassword,
    role: student.role,
  }));
  const accCol = (await connection).db().collection("Account");
  const insertedIds = (await accCol.insertMany(accounts)).insertedIds;
  return insertedIds;
}

function addUid(students, ids) {
  students.forEach((std, index) => (std.uid = ids[index]));
  return students;
}

async function saveStudents(students) {
  const stdCol = (await connection).db().collection("Student");
  return stdCol.insertMany(students);
}

function extractClasses(records) {
  return records.reduce((accumulator, record) => {
    accumulator[record.classid] = [...(accumulator[record.classid] || []), record.StudentID];
    return accumulator;
  }, {});
}

async function updateStudentListOfClass(classesAndStudentIdsMap) {
  const clsCol = (await connection).db().collection("Classes");
  const stdCol = (await connection).db().collection("Student");
  const entries = Object.entries(classesAndStudentIdsMap);
  console.log("number of class: " + entries.length);
  const opResult = entries.reduce(
    async (resultAccumulator, entry) => {
      const accum = await resultAccumulator;
      const classId = entry[0];
      const foundCls = await clsCol.findOne({ classId: classId });
      if (!foundCls) {
        console.log("not found " + classId);
        accum.notFoundClass.push(classId);
        return accum;
      }
      const students = await Promise.all(entry[1].map(async (sid) => stdCol.findOne({ studentId: sid })));
      await clsCol.updateOne({ classId: classId }, { $set: { students: students } });
      accum.updatedClass.push(classId);
      return accum;
    },
    { notFoundClass: [], updatedClass: [] }
  );
  return opResult;
}

module.exports = router;
