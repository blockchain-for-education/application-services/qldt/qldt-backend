const express = require("express");
const router = express.Router();

const makeRequestRouter = require("./make-request/make-request");
router.use(makeRequestRouter);

router.use(require("./upload-certificate/upload-certificate"));
router.use(require("./upload-attendance/upload-attendance"));
router.use(require("./upload-schedule/upload-schedule"));
router.use(require("./upload-point/upload-point"));
module.exports = router;
